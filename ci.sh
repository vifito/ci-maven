#!/bin/bash

function usc_check_requirements() {
    # Comprobar se temos dispoñible o comando java
    if [ "$JAVA_HOME" = "" ]; then
        usc_print_error "JAVA_HOME non configurado"
    else
        usc_print_debug "JAVA_HOME: $JAVA_HOME"
    fi 

    # Comprobar se temos dispoñible maven ou mavenwrapper
    USC_MAVEN_CMD=`/usr/bin/which mvn`
    if [ usc_maven_exists ]; then
        # Comprobar se existe mvnwrapper
        if [ -f "`pwd`/mvnw" ]; then
            USC_MAVEN_CMD="`pwd`/mvnw"
            usc_print_debug "USC_MAVEN_CMD: $USC_MAVEN_CMD"
        else
            usc_print_warning "Maven non dispoñible, emprégase bash scripting"
        fi
    else
        usc_print_debug "USC_MAVEN_CMD: $USC_MAVEN_CMD"
    fi

    # Comprobar se existe o ficheiro pom.xml
    USC_POM_FILE="`pwd`/pom.xml"
    if [ ! -f "$USC_POM_FILE" ]; then
        usc_print_error "pom.xml non detectado"
    fi

    # Comprobar se temos dispoñible o comando git
    USC_GIT_CMD=`which git`
    if [ "$USC_GIT_CMD" = "" ]; then
        usc_print_error "$USC_GIT_CMD non dispoñible"
    fi
}

function usc_bootstrap() {
    # Comprobar requerimentos
    usc_check_requirements

    #$USC_GIT_CMD branch -r --show-current
    USC_GIT_BRANCH=$CI_COMMIT_BRANCH
    usc_print_debug "USC_GIT_BRANCH: $USC_GIT_BRANCH"
    if [ "$USC_GIT_BRANCH" = "" ]; then
        USC_GIT_BRANCH=`$USC_GIT_CMD branch -r --show-current`
        usc_print_debug "USC_GIT_BRANCH: $USC_GIT_BRANCH"
    fi
    if [ "$USC_GIT_BRANCH" = "" ]; then
        USC_GIT_BRANCH=$CI_COMMIT_REF_NAME
        usc_print_debug "USC_GIT_BRANCH: $USC_GIT_BRANCH"
    fi

    USC_GIT_REMOTE=`$USC_GIT_CMD remote`
    usc_print_debug "USC_GIT_REMOTE: $USC_GIT_REMOTE"

    $USC_GIT_CMD config user.email vifito@gmail.com
    $USC_GIT_CMD config user.name vifito

    $USC_GIT_CMD remote remove $USC_GIT_REMOTE
    $USC_GIT_CMD remote add $USC_GIT_REMOTE https://oauth2:${TOKEN_GITLAB}@gitlab.com/vifito/ci-maven

    $USC_GIT_CMD fetch

    USC_PROJECT_VERSION=`usc_get_project_version`
    usc_print_debug "USC_PROJECT_VERSION: $USC_PROJECT_VERSION"
    
    USC_MAJOR_VERSION=`echo $USC_PROJECT_VERSION | sed -r 's/^([0-9]+)\..*?$/\1/'`
    usc_print_debug "USC_MAJOR_VERSION: $USC_MAJOR_VERSION"
    
    USC_MINOR_VERSION=`echo $USC_PROJECT_VERSION | sed -r 's/^[0-9]+\.([0-9]+)\..*?$/\1/'`
    USC_NEXT_MINOR_VERSION=$((USC_MINOR_VERSION+1))
    usc_print_debug "USC_MINOR_VERSION: $USC_MINOR_VERSION"

    USC_PATCH_VERSION=`echo $USC_PROJECT_VERSION | sed -r 's/^[0-9]+\.[0-9]+\.([0-9]+).*?$/\1/'`
    NEXT_USC_PATCH_VERSION=$((USC_PATCH_VERSION+1))
    usc_print_debug "USC_PATCH_VERSION: $USC_PATCH_VERSION"
}

function usc_print_error() {
    echo "##[error] $1"
    #exit -1
}

function usc_print_warning() {
    echo "##[warning] $1"
}

function usc_print_debug() {
    echo "##[debug] $1"
}

function usc_get_project_version() {
    # Se non está dispoñible maven empregar bash
    if [ usc_maven_exists ]; then
        # Aplanar nunha liña o xml do POM, eliminar contido <parent><version/></parent> e recuperar <version>
        cat ./pom.xml | tr -d '\n\r\t' | sed -r 's/<parent>.*?<\/parent>//' | sed -r 's/^.*?<version>(.*?)<\/version>.*?$/\1/'
    else
        $USC_MAVEN_CMD help:evaluate -q -DforceStdout -D"expression=project.version"
    fi
}

function usc_maven_exists() {
    [ "$USC_MAVEN_CMD" != "" ]
}

function usc_update_pom_version() {
    usc_print_debug "Actualizando proxecto á versión: $1" 
    if [ usc_maven_exists ]; then
        _REPLACE_EXPRESSION="s/<version>${USC_PROJECT_VERSION}<\/version>/<version>${1}<\/version>/"
        cat $USC_POM_FILE | sed -r $_REPLACE_EXPRESSION > ${USC_POM_FILE}.bak
        mv ${USC_POM_FILE}.bak ${USC_POM_FILE}
    else
        $USC_MAVEN_CMD versions:set -DgenerateBackupPoms=false -DnewVersion=$1
    fi
}

function usc_create_release() {
    # Comprobamos se estamos sobre o branch master, noutro caso lanzamos erro
    if [ "$USC_GIT_BRANCH" == "master" ]; then 
        usc_print_debug "Creando Release"

        USC_RELEASE_NAME="release/${USC_MAJOR_VERSION}.${USC_MINOR_VERSION}"
        USC_TAG_NAME="v${USC_MAJOR_VERSION}.${USC_MINOR_VERSION}.0"

        $USC_GIT_CMD checkout -b $USC_RELEASE_NAME
        usc_print_debug "USC_RELEASE_NAME: $USC_RELEASE_NAME"

        USC_NEXT_PROJECT_VERSION="${USC_MAJOR_VERSION}.${USC_MINOR_VERSION}.0"
        usc_update_pom_version $USC_NEXT_PROJECT_VERSION
        $USC_GIT_CMD commit -m "Release $USC_NEXT_PROJECT_VERSION" -a

        [[ "$USC_GIT_REMOTE" != "" ]] && $USC_GIT_CMD push $USC_GIT_REMOTE $USC_RELEASE_NAME

        $USC_GIT_CMD tag -a $USC_TAG_NAME -m "Nova release $USC_RELEASE_NAME"
        usc_print_debug "USC_TAG_NAME: $USC_TAG_NAME"
        [[ "$USC_GIT_REMOTE" != "" ]] && $USC_GIT_CMD push --tags

        # Volver ao master
        $USC_GIT_CMD checkout master

        # Seguinte versión menor SNAPSHOT
        USC_NEXT_PROJECT_VERSION="${USC_MAJOR_VERSION}.${USC_NEXT_MINOR_VERSION}.0-SNAPSHOT"
        usc_update_pom_version $USC_NEXT_PROJECT_VERSION
        
        $USC_GIT_CMD commit -m "Versión snapshot $USC_NEXT_PROJECT_VERSION" -a
        [[ "$USC_GIT_REMOTE" != "" ]] && $USC_GIT_CMD push
    else
        usc_print_error "Non estamos no branch master"
    fi
}

function usc_create_hotfix() {
    if [[ "$USC_GIT_BRANCH" = release* ]]; then
        usc_print_debug "Creando Hotfix"

        USC_RELEASE_NAME="release/${USC_MAJOR_VERSION}.${USC_MINOR_VERSION}"
        usc_print_debug "USC_RELEASE_NAME: $USC_RELEASE_NAME"
        USC_TAG_NAME="v${USC_MAJOR_VERSION}.${USC_MINOR_VERSION}.${NEXT_USC_PATCH_VERSION}"
        usc_print_debug "USC_TAG_NAME: $USC_TAG_NAME"

        $USC_GIT_CMD tag -a $USC_TAG_NAME -m "Novo hotfix $USC_TAG_NAME da release $USC_RELEASE_NAME"
        [[ "$USC_GIT_REMOTE" != "" ]] && $USC_GIT_CMD push $USC_GIT_REMOTE --tags

        # Seguinte versión menor SNAPSHOT
        USC_NEXT_PROJECT_VERSION="${USC_MAJOR_VERSION}.${USC_MINOR_VERSION}.${NEXT_USC_PATCH_VERSION}"
        usc_update_pom_version $USC_NEXT_PROJECT_VERSION
        
        $USC_GIT_CMD commit -m "Hotfix $USC_NEXT_PROJECT_VERSION" -a
        [[ "$USC_GIT_REMOTE" != "" ]] && $USC_GIT_CMD push $USC_GIT_REMOTE
     else
        usc_print_error "Non estamos nun branch release/*"
    fi
}

###############################
##                  _        ##
##  _ __ ___   __ _(_)_ __   ##
## | '_ ` _ \ / _` | | '_ \  ##
## | | | | | | (_| | | | | | ##
## |_| |_| |_|\__,_|_|_| |_| ##
##                           ##
###############################

set -x
# Comprobar requerimentos e establecer variables
usc_bootstrap

# Executar
if [ "$USC_GIT_BRANCH" == "master" ]; then 
    usc_create_release
else
    if [[ "$USC_GIT_BRANCH" = release* ]]; then
        usc_create_hotfix
    else
        usc_print_error "Non estamos nun branch release/*"
    fi
fi
set +x